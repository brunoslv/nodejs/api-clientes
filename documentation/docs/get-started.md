---
id: get-started
title: Getting Started
sidebar_label: Get Started
---

### Primeiramente navegue até o diretório da aplicação.

```
user@hostname ~/<raiz-do-projeto>/api
$ ls -ltr
total 437
-rw-r--r-- 1 Bruno 197121     18 mai 15 20:32 README.md
drwxr-xr-x 1 Bruno 197121      0 mai 15 21:07 bin      
-rw-r--r-- 1 Bruno 197121    837 mai 21 01:34 knexfile.js
drwxr-xr-x 1 Bruno 197121      0 mai 22 00:42 src
drwxr-xr-x 1 Bruno 197121      0 mai 22 01:56 node_modules
-rw-r--r-- 1 Bruno 197121 259915 mai 22 01:56 package-lock.json
-rw-r--r-- 1 Bruno 197121   1098 mai 22 01:58 package.json
-rw-r--r-- 1 Bruno 197121   6293 mai 22 02:00 jest.config.js
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:19 __tests__
```

### Rode o comando a seguir para instalar as dependências da aplicação

```
npm install
```

### Variáveis de ambiente

Devido a diferença entre sistemas operacionais, para definir variáveis de ambiente, é necessário seguir algumas regras.



Abra o arquivo package.json e altere as seguintes linhas 9, 10 e 11 de acordo com seu sistema operacional

___
__Nota:__ O arquivo já está configurado para máquinas com sistemas operacionais Windows.  
___

- __Windows__:
    ```json
    "pretest": "SET NODE_ENV=test& knex migrate:latest",
    "test": "SET NODE_ENV=test& jest --detectOpenHandles --forceExit",
    "posttest": "SET NODE_ENV=test& knex migrate:rollback --all"
    ```

- __Linux__ e __MasOS__:
    ```json
    "pretest": "NODE_ENV=test knex migrate:latest",
    "test": "NODE_ENV=test jest --detectOpenHandles --forceExit",
    "posttest": "NODE_ENV=test knex migrate:rollback --all"
    ```

### Configuração do Banco de Dados

A aplicação utiliza o MySQL como banco de dados.

- Pré-requisitos
    - Banco de dados MySQL
    - Bases de dados criadas
    - Usuário(s) com permissão de administrador (owner) das bases que serão utilizadas.

#### Exemplos para a criação da base de dados

```sql
CREATE DATABASE NOME;
```

#### Exemplos para a criação do usuário e permissionamento 

```sql
CREATE USER 'USER'@'127.0.0.1' IDENTIFIED BY 'SENHA';
GRANT ALL PRIVILEGES ON NOME_DO_DATABASE.* TO 'USER'@'127.0.0.1';
```


### Arquivo de configuração

Dentro do diretório da aplicação há dois arquivos de configuração, __.env__ e __.env.test__. Estes arquivos irão criar as variáveis de ambiente que a aplicação precisa para conectar-se ao banco de dados.

- __.env__: Arquivo de configuração utilizado para ambientes produtivos.
- __.env.test__: Arquivo de configuração utilizado para ambiente de teste.

Para isto, preencha as informações corretamente para evitar problemas futuros. 

```
APP_ADDRESS=
APP_PORT=4000

DB_HOST= 
DB_USER= 
DB_PASS=
DB_NAME=
```
___
__Cuidado:__ A variável de ambiente __APP_PORT__ refere-se a porta da aplicação. A aplicação pode não funcionar ao ser colocada em uma porta já utilizada.
___

### Migrations

Após ter configurado os passos acima, é necessário rodar as migrations.

#### Navegue até o diretório base da aplicação e execute o seguinte comando:

```json
knex migration:latest
```

___
Caso o comando __knex__ não seja encontrado, instale-o com o seguinte comando:
```
npm install knex -g
```
Agora rode o comando para executar as migrations
___

### Iniciando a aplicação

Para iniciar a aplicação em ambiente produtivo, utilize o comando:

```json
npm start
```

Acesse: __http://localhost:4000/__ para verificar se ela está funcionando.


