---
id: testes
title: Testes de Integração
sidebar_label: Testes
---

# Iniciando com os testes

Os testes foram desenvolvidos utilizando a ferramenta __jest__ juntamente com a ferramenta __supertest__ para testar o webserver.

Para executar os testes de integração é preciso primeiro configurar as variáveis de ambiente. Para mais detalhes, volte até a sessão __Get Started__.

Após realizar a configuração, execute o comando a seguir:

```
npm test
```

Ao executar este comando, todos os testes desenvolvidos serão executados automaticamente.

___
__Nota__: As migrations executam automaticamente quando o comando é disparado.



__Cuidado: Não configure a mesma base do ambiente produtivo no arquivo de configuração __.env.test__. Ao finalizar a execução dos testes, as tabelas são zeradas e não há como recuperar os dados.__
___

## Descrição dos testes

### Index

- GET
    - Should return status code 200 when is accessed

### Cliente

- GET
    - Should return the quantity of users defined by frontend in url parameter "limite"
    - Should return the quantity of users defined by frontend in url parameter "pagina"
    - Should return the user with ID defined in url query
    - Should return status code 404 when ID not exist in database
- POST
    - Should return status code 201 when create a customer with the data in body request
    - Should return status code 400 when create a customer with the wrong data in body request
    - Should return status code 409 when create a customer with a e-mail already existed in database
- PUT
    - Should return status code 200 when updated customer with data in body request
    - Should return status code 400 when try to updated a customer with invalid data in body request
    - Should return status code 404 when try to updated a customer with a ID that not existed in database
    - Should return status code 409 when try to updated a customer with a e-mail already existed in database
- DELETE
    - Should return status code 200 when a user is deleted
    - Should return status code 404 when try delete a user that not exist


## Cobertura

Ao final dos testes, é gerado um relatório disponibilizado pelo próprio __jest__ contendo informações como:

- Cobertura do código
- Quais linhas e/ou arquivos não foram testadas

Esse relatório é disponibilizado no próprio terminal de execução dos testes e em um arquivo __index.html__ localizado no diretório "`__tests__`/coverage/lcov-report/index.html"