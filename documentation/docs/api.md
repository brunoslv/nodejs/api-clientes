---
id: api
title: API
sidebar_label: API
---

## API - Clientes

API desenvolvida em Node.js juntamente com o framework Express e KnexJS para gerenciar as migrations.

## Rotas

### Index

- GET - ('/')
    - Body: {}
    - Query: ""
    - Params: ""
```json
{   
    author	"Bruno da Silva Barros"
    message	"Welcome to Customer API"
}
```

### Cliente

- GET - ('/cliente')
    - Body: {}
    - Query: "limite", "pagina"
    - Params: ""
```json
{   
    total: ""
    lista: [
        {
            id: ""
            nome: ""
            email: ""
            dataDeNascimento: "dd/mm/yyyy"
        }
    ]
}
```

- GET_BY_ID - ('/cliente/:id')
    - Body: {}
    - Query: ""
    - Params: "id"
```json  
{
    id: ""
    nome: ""
    email: ""
    dataDeNascimento: "dd/mm/yyyy"
}
```

- POST - ('/cliente')
    - Body: {nome: "", email: "", dataDeNascimento: "dd/mm/yyyy"}
    - Query: ""
    - Params: ""
```json  
{
    id: ""
    nome: ""
    email: ""
    dataDeNascimento: "dd/mm/yyyy"
}
```

- PUT - ('/cliente:id')
    - Body: {nome: "", email: "", dataDeNascimento: "dd/mm/yyyy"}
    - Query: ""
    - Params: "id"
```json  
{
    id: ""
    nome: ""
    email: ""
    dataDeNascimento: "dd/mm/yyyy"
}
```

- DELETE - ('/cliente:id')
    - Body: {}
    - Query: ""
    - Params: "id"
```json  
{
    message: "Cliente deletado com sucesso"
}
```

## Validações

Foram realizadas algumas validações com a biblioteca express-validator com o objetivo de evitar dados com formatação incorreta ou problemas ao cadastrar ou atualizar algum cliente.

Exemplo:

```javascript
validations: [
    check('nome', "Deve conter no mínimo 3 caracteres.").isLength({ min: 3 }),
    check('email', "E-mail inválido").isEmail(),
    check('dataDeNascimento', "Data de nascimento inválida.").custom(value => {
        if (!value.match(/^\d{2}\/\d{2}\/\d{4}$/)) 
            return false;
        return true;
    })
],
```