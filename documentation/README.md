This website was created with [Docusaurus](https://docusaurus.io/).
# Docusaurus 

Uma plataforma de documentação de código aberto, desenvolvida pelo time de projetos abertos do Facebook.

## Começando com Docusaurus

### Primeiramente navegue até o diretório da aplicação.

```
user@hostname ~/<raiz-do-projeto>/documentation/website
$ ls -ltr
total 614
-rw-r--r-- 1 Bruno 197121   4072 out 26  1985 README.md
-rw-r--r-- 1 Bruno 197121 349137 mai 22 02:30 package-lock.json
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:30 blog
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:30 core
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:30 pages
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:30 static
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:32 node_modules
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:32 i18n
-rw-r--r-- 1 Bruno 197121    140 mai 22 05:34 sidebars.json
-rw-r--r-- 1 Bruno 197121   3256 mai 22 05:35 siteConfig.js
-rw-r--r-- 1 Bruno 197121    425 mai 22 16:48 package.json
```

### Rode o comando a seguir para instalar as dependências da aplicação

```
npm install
```

Após instalar as dependências da aplicação, rode o seguinte comando:

```
npm start
```

Sua aplicação estará rodando na porta __3000__ de sua máquina.

___


# Documentação completa da plataforma

[website](https://docusaurus.io/).
