const app       = require('../../src/app');
const supertest = require('supertest');
const request   = supertest(app);

describe('Get index', () => {

    it('Should return status code 200 when is accessed', async (done) => {

        let res = await request.get('/');

        expect(res.status).toBe(200);

        done();

    });

});