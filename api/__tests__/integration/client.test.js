const app       = require('../../src/app');
const supertest = require('supertest');
const request   = supertest(app);

// const connection = require('../../src/database/connection');
const repository = require('../../src/repositories/clientRepository');

const utils     = require('../utils/utils');

beforeEach(async () => {
    await repository.truncate();
});

describe('GET client', () => {

    it('Should return the quantity of users defined by frontend in url parameter "limite"', async (done) => {
        
        await utils.userGenerator(5);

        let [count] = await repository.count('*');

        count = count['count(*)'];

        let limit = 8;

        let res = await request.get(`/cliente?limite=${limit}`);

        if(count <= limit){
            expect(res.body.lista.length).toBe(count);
        } else {
            expect(res.body.lista.length).toBe(limit);
        }

        expect(res.status).toBe(200);

        done();

    });

    it('Should return the quantity of users defined by frontend in url parameter "pagina"', async (done) => {
        
        await utils.userGenerator(26);

        let [count] = await repository.count('*');

        count = count['count(*)'];

        let limit = 10;
        let page  = 2;

        let res = await request.get(`/cliente?pagina=${page}`);

        if(count > 10 && page == 1){
            expect(res.body.lista.length).toBe(10);
        }

        let offset = ((page - 1) * limit);

        if(offset == 0 ){
            expect(res.body.lista[0].id).toBe(1);
        } else {
            expect(res.body.lista[0].id).toBe(offset + 1);
        }

        expect(res.status).toBe(200);

        done();

    });

    it('Should return the user with ID defined in url query', async (done) => {

        await utils.userGeneratedToComparation();

        let id = 1

        let res = await request.get(`/cliente/${id}`);

        expect(res.status).toBe(200);
        expect(res.body).toEqual({
            id: id,
            nome: "Bruno",
            email: "brunosilva2365@gmail.com",
            dataDeNascimento: "25/04/2000"
        });

        done();

    });

    it('Should return status code 404 when ID not exist in database', async (done) => {

        let id = 1

        let res = await request.get(`/cliente/${id}`);

        expect(res.status).toBe(404);

        done();

    });
    

});

describe('POST client', () => {

    it('Should return status code 201 when create a customer with the data in body request', async (done) => {

        let data = {
            nome: "Bruno da Silva Barros",
            email: "brunosilva2365@gmail.com",
            dataDeNascimento: "06/10/2000"
        };

        let res = await request.post('/cliente').send(data);

        let dataRegistered = await repository.getWhere('email', data.email);

        data.id = dataRegistered.id;

        expect(res.status).toBe(201);
        expect(res.body).toEqual(data);

        done();

    });

    it('Should return status code 400 when create a customer with the wrong data in body request', async (done) => {

        let data = {
            nome: "Bruno da Silva Barros",
            email: "brunosilva2365@gmail.com",
            dataDeNascimento: "06/17/2000"
        };

        let res = await request.post('/cliente').send(data);

        expect(res.status).toBe(400);

        done();

    });

    it('Should return status code 409 when create a customer with a e-mail already existed in database', async (done) => {

        await utils.userGeneratedToComparation();

        let data = {
            nome: "Bruno da Silva Barros",
            email: "brunosilva2365@gmail.com",
            dataDeNascimento: "06/10/2000"
        };

        let res = await request.post('/cliente').send(data);

        expect(res.status).toBe(409);
        // expect(res.body).toEqual({ message: "Cliente cadastrado com sucesso" });

        done();

    });

});

describe('PUT client', () => {
    
    it('Should return status code 200 when updated customer with data in body request', async (done) => {
       
        await utils.userGeneratedToComparation();

        let id = 1;

        let data = {
            nome: "Nome alterado",
            email: "teste@alterado.com",
            dataDeNascimento: "20/08/2003"
        };

        let res = await request.put(`/cliente/${id}`).send(data);

        data.id = id;

        expect(res.status).toBe(200);
        expect(res.body).toEqual(data);

        done();

    });

    it('Should return status code 400 when try to updated a customer with invalid data in body request', async (done) => {
       
        await utils.userGeneratedToComparation();

        let id = 1;

        let data = {
            nome: "Nome alterado",
            email: "testealterado.com",
            dataDeNascimento: "20/08/2003"
        };

        let res = await request.put(`/cliente/${id}`).send(data);

        expect(res.status).toBe(400);

        done();

    });

    it('Should return status code 404 when try to updated a customer with a ID that not existed in database', async (done) => {

        let id = 1;

        let data = {
            nome: "Nome alterado",
            email: "brunosilva2365@outlook.com",
            dataDeNascimento: "20/08/2003"
        };

        let res = await request.put(`/cliente/${id}`).send(data);

        expect(res.status).toBe(404);

        done();

    });

    it('Should return status code 409 when try to updated a customer with a e-mail already existed in database', async (done) => {
       
        await utils.userGeneratedToComparation(true);

        let id = 1;

        let data = {
            nome: "Nome alterado",
            email: "brunosilva2365@outlook.com",
            dataDeNascimento: "20/08/2003"
        };

        let res = await request.put(`/cliente/${id}`).send(data);

        expect(res.status).toBe(409);

        done();

    });

});

describe('DELETE client', () => {

    it('Should return status code 200 when a user is deleted', async (done) => {

        await utils.userGeneratedToComparation();

        let id = 1;

        let res = await request.delete(`/cliente/${id}`);
        
        expect(res.status).toBe(200);

        done();

    });

    it('Should return status code 404 when try delete a user that not exist', async (done) => {

        let id = 1;

        let res = await request.delete(`/cliente/${id}`);
        
        expect(res.status).toBe(404);

        done();

    });

});