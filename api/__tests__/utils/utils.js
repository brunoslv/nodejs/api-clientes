// const connection = require('../../src/database/connection');
const repository = require('../../src/repositories/clientRepository');
const faker = require('faker');

module.exports = {

    async userGenerator(quantity){

        for(let i=0; i < quantity; i++){

            await repository.post({
                nome: faker.name.findName(),
                email: faker.internet.email(),
                dataDeNascimento: faker.date.past(10)
            });

        }

    },

    async userGeneratedToComparation(aux = false){

        await repository.post({
            nome: "Bruno",
            email: "brunosilva2365@gmail.com",
            dataDeNascimento: "2000/04/25"
        })

        if(aux)
            await repository.post({
                nome: "Bruno",
                email: "brunosilva2365@outlook.com",
                dataDeNascimento: "2011/04/29"
            });

    }

}