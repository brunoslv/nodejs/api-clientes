# API - Clientes

### Primeiramente navegue até o diretório da aplicação.

```
user@hostname ~/<raiz-do-projeto>/api
$ ls -ltr
total 437
-rw-r--r-- 1 Bruno 197121     18 mai 15 20:32 README.md
drwxr-xr-x 1 Bruno 197121      0 mai 15 21:07 bin      
-rw-r--r-- 1 Bruno 197121    837 mai 21 01:34 knexfile.js
drwxr-xr-x 1 Bruno 197121      0 mai 22 00:42 src
drwxr-xr-x 1 Bruno 197121      0 mai 22 01:56 node_modules
-rw-r--r-- 1 Bruno 197121 259915 mai 22 01:56 package-lock.json
-rw-r--r-- 1 Bruno 197121   1098 mai 22 01:58 package.json
-rw-r--r-- 1 Bruno 197121   6293 mai 22 02:00 jest.config.js
drwxr-xr-x 1 Bruno 197121      0 mai 22 02:19 __tests__
```

### Rode o comando a seguir para instalar as dependências da aplicação

```
npm install
```

### Variáveis de ambiente

Devido a diferença entre sistemas operacionais, para definir variáveis de ambiente, é necessário seguir algumas regras.



Abra o arquivo package.json e altere as seguintes linhas 9, 10 e 11 de acordo com seu sistema operacional

___
__Nota:__ O arquivo já está configurado para máquinas com sistemas operacionais Windows.  
___

- __Windows__:
    ```json
    "pretest": "SET NODE_ENV=test& knex migrate:latest",
    "test": "SET NODE_ENV=test& jest --detectOpenHandles --forceExit",
    "posttest": "SET NODE_ENV=test& knex migrate:rollback --all"
    ```

- __Linux__ e __MasOS__:
    ```json
    "pretest": "NODE_ENV=test knex migrate:latest",
    "test": "NODE_ENV=test jest --detectOpenHandles --forceExit",
    "posttest": "NODE_ENV=test knex migrate:rollback --all"
    ```

### Configuração do Banco de Dados

A aplicação utiliza o MySQL como banco de dados.

- Pré-requisitos
    - Banco de dados MySQL
    - Bases de dados criadas
    - Usuário(s) com permissão de administrador (owner) das bases que serão utilizadas.

#### Exemplos para a criação da base de dados

```sql
CREATE DATABASE NOME;
```

#### Exemplos para a criação do usuário e permissionamento 

```sql
CREATE USER 'USER'@'127.0.0.1' IDENTIFIED BY 'SENHA';
GRANT ALL PRIVILEGES ON NOME_DO_DATABASE.* TO 'USER'@'127.0.0.1';
```


### Arquivo de configuração

Dentro do diretório da aplicação há dois arquivos de configuração, __.env__ e __.env.test__. Estes arquivos irão criar as variáveis de ambiente que a aplicação precisa para conectar-se ao banco de dados.

- __.env__: Arquivo de configuração utilizado para ambientes produtivos.
- __.env.test__: Arquivo de configuração utilizado para ambiente de teste.

Para isto, preencha as informações corretamente para evitar problemas futuros. 

```
APP_ADDRESS=
APP_PORT=4000

DB_HOST= 
DB_USER= 
DB_PASS=
DB_NAME=
```
___
__Cuidado:__ A variável de ambiente __APP_PORT__ refere-se a porta da aplicação. A aplicação pode não funcionar ao ser colocada em uma porta já utilizada.
___

### Migrations

Após ter configurado os passos acima, é necessário rodar as migrations.

#### Navegue até o diretório base da aplicação e execute o seguinte comando:

```json
knex migration:latest
```

___
Caso o comando __knex__ não seja encontrado, instale-o com o seguinte comando:
```
npm install knex -g
```
Agora rode o comando para executar as migrations
___

### Iniciando a aplicação

Para iniciar a aplicação em ambiente produtivo, utilize o comando:

```json
npm start
```

Acesse: __http://localhost:4000/__ para verificar se ela está funcionando.

# API

API desenvolvida em Node.js juntamente com o framework Express e KnexJS para gerenciar as migrations.

## Rotas

### Index

- GET - ('/')
    - Body: {}
    - Query: ""
    - Params: ""
```json
{   
    author	"Bruno da Silva Barros"
    message	"Welcome to Customer API"
}
```

### Cliente

- GET - ('/cliente')
    - Body: {}
    - Query: "limite", "pagina"
    - Params: ""
```json
{   
    total: ""
    lista: [
        {
            id: ""
            nome: ""
            email: ""
            dataDeNascimento: "dd/mm/yyyy"
        }
    ]
}
```

- GET_BY_ID - ('/cliente/:id')
    - Body: {}
    - Query: ""
    - Params: "id"
```json  
{
    id: ""
    nome: ""
    email: ""
    dataDeNascimento: "dd/mm/yyyy"
}
```

- POST - ('/cliente')
    - Body: {nome: "", email: "", dataDeNascimento: "dd/mm/yyyy"}
    - Query: ""
    - Params: ""
```json  
{
    id: ""
    nome: ""
    email: ""
    dataDeNascimento: "dd/mm/yyyy"
}
```

- PUT - ('/cliente:id')
    - Body: {nome: "", email: "", dataDeNascimento: "dd/mm/yyyy"}
    - Query: ""
    - Params: "id"
```json  
{
    id: ""
    nome: ""
    email: ""
    dataDeNascimento: "dd/mm/yyyy"
}
```

- DELETE - ('/cliente:id')
    - Body: {}
    - Query: ""
    - Params: "id"
```json  
{
    message: "Cliente deletado com sucesso"
}
```

## Validações

Foram realizadas algumas validações com a biblioteca express-validator com o objetivo de evitar dados com formatação incorreta ou problemas ao cadastrar ou atualizar algum cliente.

Exemplo:

```javascript
validations: [
    check('nome', "Deve conter no mínimo 3 caracteres.").isLength({ min: 3 }),
    check('email', "E-mail inválido").isEmail(),
    check('dataDeNascimento', "Data de nascimento inválida.").custom(value => {
        if (!value.match(/^\d{2}\/\d{2}\/\d{4}$/)) 
            return false;
        return true;
    })
],
```

# Testes

Os testes foram desenvolvidos utilizando a ferramenta __jest__ juntamente com a ferramenta __supertest__ para testar o webserver.

Para executar os testes de integração é preciso primeiro configurar as variáveis de ambiente. Para mais detalhes, volte até a sessão __Get Started__.

Após realizar a configuração, execute o comando a seguir:

```
npm test
```

Ao executar este comando, todos os testes desenvolvidos serão executados automaticamente.

___
__Nota__: As migrations executam automaticamente quando o comando é disparado.
___

___
__Cuidado: Não configure a mesma base do ambiente produtivo no arquivo de configuração __.env.test__. Ao finalizar a execução dos testes, as tabelas são zeradas e não há como recuperar os dados.__
___

## Descrição dos testes

### Index

- GET
    - Should return status code 200 when is accessed

### Cliente

- GET
    - Should return the quantity of users defined by frontend in url parameter "limite"
    - Should return the quantity of users defined by frontend in url parameter "pagina"
    - Should return the user with ID defined in url query
    - Should return status code 404 when ID not exist in database
- POST
    - Should return status code 201 when create a customer with the data in body request
    - Should return status code 400 when create a customer with the wrong data in body request
    - Should return status code 409 when create a customer with a e-mail already existed in database
- PUT
    - Should return status code 200 when updated customer with data in body request
    - Should return status code 400 when try to updated a customer with invalid data in body request
    - Should return status code 404 when try to updated a customer with a ID that not existed in database
    - Should return status code 409 when try to updated a customer with a e-mail already existed in database
- DELETE
    - Should return status code 200 when a user is deleted
    - Should return status code 404 when try delete a user that not exist

## Cobertura

Ao final dos testes, é gerado um relatório disponibilizado pelo próprio __jest__ contendo informações como:

- Cobertura do código
- Quais linhas e/ou arquivos não foram testadas

Esse relatório é disponibilizado no próprio terminal de execução dos testes e em um arquivo __index.html__ localizado no diretório "`__tests__`/coverage/lcov-report/index.html"