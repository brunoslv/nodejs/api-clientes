
exports.up = function(knex) {
    return knex.schema.createTable('cliente', function(table) {
        table.increments('id').primary();
        table.string('nome').notNullable();
        table.string('email').unique().notNullable();
        table.date('dataDeNascimento').notNullable();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('cliente');
};
