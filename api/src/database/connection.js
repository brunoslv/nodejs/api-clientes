const knex          = require('knex');
const knexConfig    = require('../../knexfile');

const schema     = process.env.NODE_ENV === "test" ? knexConfig.test : knexConfig.development;

const connection = knex(schema);

module.exports = connection;
