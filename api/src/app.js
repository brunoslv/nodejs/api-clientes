const express       = require('express');
const cors          = require('cors');
const app           = express();
const bodyParser    = require('body-parser');

const indexRoute    = require('./routes/indexRoute');
const clientRoute   = require('./routes/clientRoute');

app.use(cors());
app.use(bodyParser.json());
app.use('/', indexRoute);
app.use('/cliente', clientRoute);

module.exports = app;