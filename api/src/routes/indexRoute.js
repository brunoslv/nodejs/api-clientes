'use strict';

const express   = require('express');
const router    = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).json({ author: "Bruno da Silva Barros", message: "Welcome to Customer API" });
});

module.exports = router;