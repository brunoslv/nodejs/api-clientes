'use strict';

const express   = require('express');
const router    = express.Router();

const clientController  = require('../controllers/clientController');

router.get('/all', clientController.getAll);
router.get('/', clientController.get);
router.get('/:id', clientController.getById);
router.post('/', clientController.create.validations, clientController.create.post);
router.put('/:id', clientController.update.validations, clientController.update.put);
router.delete('/:id', clientController.delete);

module.exports = router;