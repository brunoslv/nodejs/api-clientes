'use strict';

const dateFormat = require('dateformat');

module.exports = {

    formatDate(date){
        let dtNasc = date;

        dtNasc = dtNasc.split('/');

        dtNasc = `${dtNasc[2]}/${dtNasc[1]}/${dtNasc[0]}`;
        
        try{
            dateFormat(dtNasc, "dd/mm/yyyy");
            return dtNasc;
        } catch(e) {
            return false;
        }
    }

}