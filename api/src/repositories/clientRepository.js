'use strict';

const connection = require('../database/connection');
const dateFormat = require('dateformat');

module.exports = {

    async getAll(){
        let response = await connection('cliente').select(connection.raw('id, nome, email, DATE_FORMAT(dataDeNascimento, "%d/%m/%Y") as dataDeNascimento'));
        return response;
    },

    async get(limit, page) {        
        let response = await connection('cliente').limit(limit).offset((page - 1) * limit).select('id').select('nome').select('email').select(connection.raw('DATE_FORMAT(dataDeNascimento, "%d/%m/%Y") as dataDeNascimento'));
        return response;
    },

    async count() {
        let response = await connection('cliente').count();
        return response;
    },

    async getById(id) {
        let response = await connection('cliente').select('id').select('nome').select('email').select(connection.raw('DATE_FORMAT(dataDeNascimento, "%d/%m/%Y") as dataDeNascimento')).where('id', id).first();
        return response;
    },

    async getWhere(column, value) {
        let response = await connection('cliente').select('id').select('nome').select('email').select(connection.raw('DATE_FORMAT(dataDeNascimento, "%d/%m/%Y") as dataDeNascimento')).where(column, value).first();
        return response;
    },

    async post(data) {
        await connection('cliente').insert({
            nome: data.nome,
            email: data.email,
            dataDeNascimento: dateFormat(data.dataDeNascimento, "yyyy/mm/dd")
        });
    },

    async put(id, data) {
        let response = await connection('cliente').where('id', id).update(data);
        return response;
    },

    async delete(id) {
        let response = await connection('cliente').where('id', id).delete();
        return response;
    },

    async truncate() {
        await connection('cliente').truncate();
    }

}
