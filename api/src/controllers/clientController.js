'use strict';

const repository    = require('../repositories/clientRepository');
const { check, validationResult } = require('express-validator');
const { formatDate } = require('../utils/utils');

const errorFormatter =  ({ location, msg, param, value, nestedErrors }) => {
    return `${msg}`;
};

module.exports = {

    async getAll(req, res, next) {

        try{
            let data = await repository.getAll();
            res.status(200).send(data);
        } catch(e) {
            console.log(e);
            res.status(500).json({ message: "Erro ao processar requisição" });
        }
    },

    async get(req, res, next) {

        let limit   = req.query.limite == undefined ? 10 : req.query.limite;
        let page    = req.query.pagina == undefined ? 1 : req.query.pagina;

        try{
            let [count] = await repository.count();
            let data = await repository.get(limit, page);
            res.status(200).json({ total: count['count(*)'], lista: data });
        } catch(e) {
            console.log(e);
            res.status(500).json({ message: "Erro ao processar requisição" });
        }
    },

    async getById(req, res, next) {
        try{
            let data = await repository.getById(req.params.id);

            // console.log(Object.entries(data));
            // console.log(data);

            if(data === undefined) {
                res.status(404).json({ message: `Nenhum cliente encontrado com ID ${req.params.id}` });
                return;
            }

            res.status(200).json(data);
        } catch(e) {
            console.log(e);
            res.status(500).json({ message: "Erro ao processar requisição" });
        }
    },

    create: {
        validations: [
            check('nome', "Deve conter no mínimo 3 caracteres.").isLength({ min: 3 }),
            check('email', "E-mail inválido").isEmail(),
            check('dataDeNascimento', "Data de nascimento inválida.").custom(value => {
                if (!value.match(/^\d{2}\/\d{2}\/\d{4}$/)) 
                    return false;
                return true;
            })
        ],

        post: async (req, res, next) =>{

            const schemaErrors = validationResult(req).formatWith(errorFormatter);

            if (!schemaErrors.isEmpty()) {
                return res.status(400).json({ errors: schemaErrors.mapped() });
            }

            let dtNasc = formatDate(req.body.dataDeNascimento);

            if(!dtNasc){
                return res.status(400).json({ errors: { dataDeNascimento: "Data de nascimento inválida" } });
            }

            req.body.dataDeNascimento = dtNasc;

            try{

                let findEmail = await repository.getWhere('email', req.body.email);

                if(findEmail){
                    return res.status(409).json({ message: "E-mail já cadastrado" });
                }
                
                let data = await repository.post(req.body);
                res.status(201).json(await repository.getWhere('email', req.body.email));
            } catch(e){
                console.log(e);
                res.status(500).json({ message: "Erro ao processar requisição" });
            }
        }
    },

    update: {
        validations: [
            check('nome', "Deve conter no mínimo 3 caracteres.").optional().isLength({ min: 3 }),
            check('email', "E-mail inválido").optional().isEmail(),
            check('dataDeNascimento', "Data de nascimento inválida.").optional().custom(value => {
                if (!value.match(/^\d{2}\/\d{2}\/\d{4}$/)) 
                    return false;
                return true;
            })
        ],

        async put(req, res, next) {

            const schemaErrors = validationResult(req).formatWith(errorFormatter);

            if (!schemaErrors.isEmpty()) {
                return res.status(400).json({ errors: schemaErrors.mapped() });
            }

            if(req.body.dataDeNascimento){
                let dtNasc = formatDate(req.body.dataDeNascimento);

                if(!dtNasc){
                    return res.status(400).json({ errors: { dataDeNascimento: "Data de nascimento inválida" } });
                }

                req.body.dataDeNascimento = dtNasc;
            }
            
            try{

                let dataId = await repository.getById(req.params.id);

                if(dataId === undefined) {
                    res.status(404).json({ message: `Nenhum cliente encontrado com ID ${req.params.id}` });
                    return;
                }

                let findEmail = await repository.getWhere('email', req.body.email);

                if((findEmail) && (findEmail.id != req.params.id)){
                    return res.status(409).json({ message: "E-mail já cadastrado" });
                }

                let data = await repository.put(req.params.id, req.body);
                res.status(200).send(await repository.getById(req.params.id));
            } catch(e) {
                res.status(500).json({ message: "Erro ao processar requisição" });
            }
        }
    },
    
    async delete(req, res, next) {

        try{
            let data = await repository.getById(req.params.id);

            if(data === undefined) {
                res.status(404).json({ message: `Nenhum cliente encontrado com ID ${req.params.id}` });
                return;
            }
            await repository.delete(req.params.id);
            res.status(200).json({ message: "Cliente deletado com sucesso" });
        } catch(e) {
            res.status(500).json({ message: "Erro ao processar requisição", data: e });
        }
    }

}