'use strict';

require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
});

const app   = require('../src/app');
const http  = require('http');

const hostname  = process.env.APP_ADDRESS;
const port      = process.env.APP_PORT;

const server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});