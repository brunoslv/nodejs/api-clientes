# API

## Links para acesso à API e Documentação:

### API

- <http://52.67.25.114:4000>

___
__Nota:__ A documentação da API também está disponível no README.md no diretório './api'.
___

### Documentação

- <http://52.67.25.114:3000>

___
__Nota:__ A documentação do Docusaurus também está disponível no README.md no diretório './documentation'.
___

__As máquinas estarão acessíveis todos os dias das 09:00 às 18:00.__